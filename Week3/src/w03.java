import java.util.*;


public class w03 {
    public static void main (String[] args) {

        System.out.println("\nException Handling and Data Validation\n\n");

        int num1 = 0;
        int num2 = 0;
        System.out.println("Gather Two Numbers");
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter # 1 ");
        String snum1 = sc.nextLine();
        num1 = Integer.parseInt(snum1);
        System.out.println("Enter # 2 ");
        String snum2 = sc.nextLine();
        num2 = Integer.parseInt(snum2);

        divide(num1,num2);


    }

    public static void divide(int x, int y) {
       try {
           int z = x/y;
           System.out.println(x + " divided by " + y + " = " + z);
       }
       catch (ArithmeticException e){
           System.out.println("You can't divide by a zero! \nPlease Try Again");

           int num1 = 0;
           int num2 = 0;
           System.out.println("\nGather Two Numbers\n\n");
           Scanner sc = new Scanner(System.in);
           System.out.println("Enter # 1 ");
           String snum1 = sc.nextLine();
           num1 = Integer.parseInt(snum1);
           System.out.println("Enter # 2 ");
           String snum2 = sc.nextLine();
           num2 = Integer.parseInt(snum2);

           divide(num1,num2);


       }

    }
}
