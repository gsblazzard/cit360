import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JSON {


    public static String stringToJSON(String string) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "";

        try {
            Users users = mapper.readValue(string, Users.class);
            jsonString = mapper.writeValueAsString(users);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return jsonString;
    }

    public static Users JSONToUsers(String s) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        Users users = null;

        try {
            users = mapper.readValue(s, Users.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return users;
    }

    public static void main(String[] args) throws IOException {
        String json = HTTP.getHttpContentandConvertToJSON("https://brockblaze.github.io/CIT360/json/user.txt");
        System.out.println(json);

        Users users = JSON.JSONToUsers(json);
        System.out.println(users);
    }
}