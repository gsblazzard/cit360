public class Users {
    private String username;
    private String email;
    private Integer pin;

    public String getUsername() { return username; }

    public void setUsername() { this.username = username; }

    public String getEmail() { return email;  }

    public void setEmail(String name) {
        this.email = name;
    }

    public Integer getPin() {
        return pin;
    }

    public void setPin(Integer pin) {
        this.pin = pin;
    }

    public String toString() {
        return "Username: " + username + " Email: " + email + " Pin: " + pin;
    }
}
