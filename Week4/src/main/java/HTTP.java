import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTP {


    public static String getHttpContentandConvertToJSON(String string) throws IOException {

        String Content = "";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line= reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }

            Content = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        String json = JSON.stringToJSON(Content);

        return json;
    }
}
