import java.util.*;

public class Collections {
    public static void main (String[] args) {

        System.out.println("\nCollections Examples\n\n");

        System.out.println("---Queue---\n A Queue behaves in that whatever entered first will be removed first.\n");
        Queue<Integer> q = new LinkedList<>();
        q.add(3);
        q.add(6);
        q.add(9);
        q.add(12);
        q.add(15);
        System.out.println("The queue is " + q);
        int num1 = q.remove();
        System.out.println("The element removed from the start is " + num1);
        System.out.println("The queue after using the remove function is " + q);
        int head = q.peek();
        System.out.println("The start of the queue is now " + head);
        int size = q.size();
        System.out.println("The size of the queue is now " + size);


        System.out.println("\n\n---Set---\n A HashSet uses hashing which means that every data entry has to be unique or it will not be added like how Monson is only added once.");

        Set s = new HashSet();
        s.add("Holland");
        s.add("Packer");
        s.add("Monson");
        s.add("Monson");

        for (Object str : s) {
            System.out.println((String) str);
        }


        System.out.println("\n\n---Tree---\n A TreeMap has both keys and values and it is sorted in an ascending order, like 1, 2, 3.\n");


        /* This is how to declare TreeMap */
        TreeMap<Integer, String> t = new TreeMap<Integer, String>();

        /*Adding elements to TreeMap*/
        t.put(12, "York");
        t.put(9, "Fairview");
        t.put(30, "Centerville");
        t.put(21, "Oakland");
        t.put(3, "Spring Lake");

        /* Display content using Iterator*/
        Set set1 = t.entrySet();
        Iterator iterator = set1.iterator();
        while(iterator.hasNext()) {
            Map.Entry me = (Map.Entry)iterator.next();
            System.out.print("Key "+ me.getKey() + " Value ");
            System.out.println(me.getValue());
        }


        System.out.println("\n\n---List---\n An ArrayList can be modified, making it different from an Array");
        List l = new ArrayList();
        l.add("New York City");
        l.add("Chicago");
        l.add("Boise");
        l.add("Lewiston");
        l.add("Spokane");
        l.add("Spirit Lake");
        l.add("Salt Lake City");
        Scanner scanner = new Scanner(System.in);
        Integer c = 0;

        while(c < 1) {
            System.out.println("Put in a City = ");
            String vgName = scanner.nextLine();

            if (vgName.length() > 0) {
                l.add(vgName);
                c++;
            } else {
                System.out.println("You need to name a city\n");

            }
        }
        l.forEach(n -> {
            System.out.println(n);
        });
    }
}
