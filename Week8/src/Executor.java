import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Executor {
    public static void main(String[] args) {
        ExecutorService myService = Executors.newFixedThreadPool(3);

        CongruentCalculator c1 = new CongruentCalculator("Addition Calculation",1,2,2);
        CongruentCalculator c2 = new CongruentCalculator("Subtraction Calculation",2,2,2);
        CongruentCalculator c3 = new CongruentCalculator("Multiplication Calculation",3,2,2);
        CongruentCalculator c4 = new CongruentCalculator("Division Calculation",4,2,2);
        CongruentCalculator c5 = new CongruentCalculator("Subtraction Calculation",2,25,7);
        CongruentCalculator c6 = new CongruentCalculator("Multiplication Calculation",3,25,4);

        myService.execute(c1);
        myService.execute(c2);
        myService.execute(c3);
        myService.execute(c4);
        myService.execute(c5);
        myService.execute(c6);

        myService.shutdown();
    }
}
