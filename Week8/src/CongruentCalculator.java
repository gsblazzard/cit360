public class CongruentCalculator implements Runnable {
    private String name;
    private int type;
    private int n1;
    private int n2;

    public CongruentCalculator(String name, int type, int n1, int n2) {
        this.name = name;
        this.type = type;
        this.n1 = n1;
        this.n2 = n2;
    }

    @Override
    public void run() {
        int a;
        if (type == 1) {
            a = n1 + n2;
            System.out.println("\n" + name + "\n The sum of " + n1 + " and " + n2 + " is " + a);
        }
        if (type == 2) {
            a = n1 - n2;
            System.out.println("\n" + name + "\n The difference of " + n1 + " and " + n2 + " is " + a);
        }
        if (type == 3) {
            a = n1 * n2;
            System.out.println("\n" + name + "\n The product of " + n1 + " and " + n2 + " is " + a);
        }
        if (type == 4) {
            a = n1 / n2;
            System.out.println("\n" + name + "\n The quotient of " + n1 + " and " + n2 + " is " + a);
        }
    }
}
