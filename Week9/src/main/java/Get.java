import org.hibernate.Session;
import org.hibernate.SessionFactory;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import javax.persistence.Query;

public class Get {
    SessionFactory factory = null;
    Session session = null;

    private static Get single_instance = null;

    private Get()
    {
        factory = Hibernate.getSessionFactory();
    }

    public static Get getInstance()
    {
        if (single_instance == null) {
            single_instance = new Get();
        }

        return single_instance;
    }

    public List<Property> getProperties() {
        try {
            session = factory.openSession();
            session .getTransaction().begin();
            String sql = "from Property";
            List<Property> p = (List<Property>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return p;
        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }


    public Property getProperty(int propertyId) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Property where id=" + Integer.toString(propertyId);
            Property p = (Property)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return p;
        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
}
