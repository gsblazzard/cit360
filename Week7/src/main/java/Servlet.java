import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet",urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><style>\n" +
                "    body {background: darkseagreen;\n" +
                "          margin: auto;\n" +
                "          width: 500px;}\n" +
                "    h1 {font-size: 2rem;\n" +
                "        color: azure}\n" +
                "</style>");
        String name = request.getParameter("name");
        String address = request.getParameter("address");
        String price = request.getParameter("price");
        out.println("<h1>New Property</h1>");
        out.println("<p>Realtor: " + name + "</p>");
        out.println("<p>Address: " + address + "</p>");
        out.println("<p>Price: $" + price + "</p>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Property info not available");
    }
}
