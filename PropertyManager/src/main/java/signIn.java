import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "signIn", urlPatterns={"/signIn"})
public class signIn extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer x = 0;
        Integer y;

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        Get test = Get.getInstance();

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        if(test.CheckUsername(username, password))
        {
            List<Property> p = test.getProperties(Get.t);

            out.println("<html><head><title>Signed In!</title><link rel=\"stylesheet\" href=\"styles/main.css\"></head><body><h1>Property</h1>\n" +
                    "<nav><a href=\"index.jsp\">Home</a> <a href=\"contact.html\">Contact</a> <a href=\"about.html\">About</a></nav>");
            out.println("<main><h1>Welcome " + username +
                    "</h1><p>Properties</p>");

            for (Property i : p) {
                out.println("<p>" + i + "</p>");
            }

            out.println("<p><form action=\"./add.html\" method=\"post\">" +
                    "        <input type=\"submit\" value=\"Add\">" +
                    "    </form></p>");

            out.println("<p><form action=\"Delete\" method=\"post\">");
            out.println("<select name=\"selection\")");

            List<Property> p1 = test.getProperties(Get.t);

            for (Property i : p1) {
                x = i.getId();
                out.println("<option value=\"" + x + "\">Property " + x + "</option>");
            }

            out.println("</select><input type=\"submit\" value=\"Delete\">");
            out.println("</form></p>");

            out.println("</main></body><footer>Garrett Blazzard | CIT 360 Final | December 2020</footer></html>");
        }
        else
        {
            out.println("<html><head><title>Error</title><link rel=\"stylesheet\" href=\"styles/main.css\"></head><body><h1>Realtor Sign In</h1>\n" +
                    "<nav><a href=\"index.jsp\">Home</a> <a href=\"contact.html\">Contact</a> <a href=\"about.html\">About</a></nav>");
            out.println("<main><p>Wrong Username or password</p> " +
                    "<p>Please <a href=\"./signin.html\">try again</a></p></main>");
            out.println("</body><footer>\n" +
                    "    Garrett Blazzard | CIT 360 Final | December 2020\n" +
                    "</footer></html>");
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("No Login Information");
    }
}
