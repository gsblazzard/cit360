import javax.persistence.*;

@Entity
@Table(name = "realtor")

public class Realtor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "realtor_id")
    private int id;

    @Column(name = "realtor_name")
    private String name;

    @Column(name = "realtor_username")
    private String username;

    @Column(name = "realtor_password")
    private String password;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Realtor: " + name;
    }
}
