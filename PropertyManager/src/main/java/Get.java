import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Scanner;

public class Get {
    SessionFactory factory = null;
    Session session = null;

    private static Get single_instance = null;
    public Property selProperty;
    public Realtor activeRealtor;

    public static String t;

    Get()
    {
        factory = hibernate.getSessionFactory();
    }

    public static Get getInstance()
    {
        if (single_instance == null) {
            single_instance = new Get();
        }

        return single_instance;
    }

    public boolean CheckUsername(String name, String password) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Realtor where username = " + "'" + name + "'";
            Realtor r = (Realtor)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            t = r.getName();
            if (r.getPassword().equals(password))
            {
                activeRealtor = r;
                return true;
            }
            else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return false;
        } finally {
            session.close();
        }
    }

    public Realtor getRealtor(int id) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Realtor where id = " + Integer.toString(id);
            Realtor r = (Realtor)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return r;
        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Property getProperty(int id) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Property where id = " + Integer.toString(id);
            Property p = (Property)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            selProperty = p;
            return p;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public List<Realtor> getRealtors() {
        try {
            session = factory.openSession();
            session .getTransaction().begin();
            String sql = "from Realtor";
            List<Realtor> r = (List<Realtor>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return r;
        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public List<Property> getProperties(String name) {
        try {
            session = factory.openSession();
            session .getTransaction().begin();
            String sql = "from Property where realtor = " + "'" + name + "'";
            List<Property> p = (List<Property>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return p;
        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Property addProperty(Property p){

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            session.save(p);
            session.getTransaction().commit();
            return p;
        }catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }

    public Property deleteProperty(Property p){

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            session.delete(p);
            session.getTransaction().commit();
            return p;
        }catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }

    public static void main(String[] args) {

        Get test = Get.getInstance();
        Integer selection;
        Integer exit = 0;


        Scanner s = new Scanner(System.in);

        while (exit == 0) {
            System.out.println("\n\nWhat Realtor do you want to select");
            try {
                selection = s.nextInt();
                if (selection == 1 || selection == 2 || selection == 3) {
                    System.out.println("\nYou selected the Realtor with the Id of: " + selection);
                    System.out.println(test.getRealtor(selection));
                    exit++;
                } else {
                    System.out.println("\nPlease select an available Realtor id number");
                }
            } catch (Exception e) {
                System.err.println(e.toString());
                selection = 0;
            }
        }
    }
}