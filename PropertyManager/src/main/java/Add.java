import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "Add", urlPatterns={"/Add"})
public class Add extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String address = request.getParameter("address");
        String stringPrice = request.getParameter("price");
        Integer price = Integer.parseInt(stringPrice);
        Integer x = 0;
        Get g = Get.getInstance();
        Realtor r = new Realtor();
        r = g.activeRealtor;

        Property p = new Property();
        p.setAddress(address);
        p.setPrice(price);
        p.setRealtor(r.getName());


        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        if (g.addProperty(p) != null) {

            List<Property> p1 = g.getProperties(Get.t);

            out.println("<html><head><title>Signed In!</title><link rel=\"stylesheet\" href=\"styles/main.css\"></head><body onload=\"myFunction()\"><h1>Property Manager</h1>\n" +
                    "<nav><a href=\"index.jsp\">Home</a> <a href=\"contact.html\">Contact</a> <a href=\"about.html\">About</a></nav><main>");

            out.println("<script>function myFunction() { alert(\"Property Added\"); }</script>");


            out.println("<h1>Welcome " + r.getName() +
                    "</h1><p>Properties</p>");

            for (Property i : p1) {
                out.println("<p>" + i + "</p>");
            }

            out.println("<p><form action=\"./add.html\" method=\"post\">" +
                    "        <input type=\"submit\" value=\"Add\">" +
                    "    </form></p>");

            out.println("<p><form action=\"Delete\" method=\"post\">");
            out.println("<select name=\"selection\")");

            for (Property i : p1) {
                x = i.getId();
                out.println("<option value=\"" + x + "\">Property " + x + "</option>");
            }

            out.println("</select><input type=\"submit\" value=\"Delete\">");
            out.println("</form></p>");

            out.println("</main></body><footer>Garrett Blazzard | CIT 360 Final | December 2020</footer></html>");

        }
        else {
            out.println("<html><head><title>Signed In!</title><link rel=\"stylesheet\" href=\"styles/main.css\"></head><body><h1>Realtor Sign In</h1>\n" +
                    "<nav><a href=\"index.jsp\">Home</a> <a href=\"contact.html\">Contact</a> <a href=\"about.html\">About</a></nav><main>");

            out.println("<h1>Property Not added</h1>");
            out.println("<p>There was an error when adding the property</p>");
            out.println("<p>Please <a href=\"./add.html\">try again</a></p>");

        }
        out.println("</main></body><footer>Garrett Blazzard | CIT 360 Final | December 2020</footer></html>");

    }
}
