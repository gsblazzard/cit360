import javax.persistence.*;

@Entity
@Table(name = "property")
public class Property {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "property_id")
    private int id;

    @Column(name = "property_address")
    private String address;

    @Column(name = "property_price")
    private int price;

    @Column(name = "property_realtor")
    private String realtor;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getRealtor() {
        return realtor;
    }

    public void setRealtor(String realtor) {
        this.realtor = realtor;
    }

    @Override
    public String toString() {
        return "Property " + id + ": " + address + " $" + price + " - " + realtor;
    }
}
