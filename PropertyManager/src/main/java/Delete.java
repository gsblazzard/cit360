import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "Delete", urlPatterns = {"/Delete"})
public class Delete extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sel = request.getParameter("selection");
        Integer selection = Integer.parseInt(sel);
        Integer x = 0;
        Get g = Get.getInstance();
        Realtor r = new Realtor();
        r = g.activeRealtor;

        g.getProperty(selection);


        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        if (g.deleteProperty(g.selProperty) != null) {

            List<Property> p1 = g.getProperties(Get.t);

            out.println("<html><head><title>Signed In!</title><link rel=\"stylesheet\" href=\"styles/main.css\"></head><body onload=\"myFunction()\"><h1>Property Manager</h1>\n" +
                    "<nav><a href=\"index.jsp\">Home</a> <a href=\"contact.html\">Contact</a> <a href=\"about.html\">About</a></nav><main>");

            out.println("<script>function myFunction() { alert(\"Property Deleted\"); }</script>");


            out.println("<h1>Welcome " + r.getName() +
                    "</h1><p>Properties</p>");

            for (Property i : p1) {
                out.println("<p>" + i + "</p>");
            }

            out.println("<p><form action=\"./add.html\" method=\"post\">" +
                    "        <input type=\"submit\" value=\"Add\">" +
                    "    </form></p>");

            out.println("<p><form action=\"Delete\" method=\"post\">");
            out.println("<select name=\"selection\")");

            for (Property i : p1) {
                x = i.getId();
                out.println("<option value=\"" + x + "\">Property " + x + "</option>");
            }

            out.println("</select><input type=\"submit\" value=\"Delete\">");
            out.println("</form></p>");

            out.println("</main></body><footer>Garrett Blazzard | CIT 360 Final | December 2020</footer></html>");

        }
        else {
            List<Property> p1 = g.getProperties(Get.t);

            out.println("<html><head><title>Signed In!</title><link rel=\"stylesheet\" href=\"styles/main.css\"></head><body onload=\"myFunction()\"><h1>Property Manager</h1>\n" +
                    "<nav><a href=\"index.jsp\">Home</a> <a href=\"contact.html\">Contact</a> <a href=\"about.html\">About</a></nav><main>");

            out.println("<script>function myFunction() { alert(\"ERROR - Property was not Deleted\"); }</script>");


            out.println("<h1>Welcome " + r.getName() +
                    "</h1><p>Properties</p>");

            for (Property i : p1) {
                out.println("<p>" + i + "</p>");
            }

            out.println("<p><form action=\"./add.html\" method=\"post\">" +
                    "        <input type=\"submit\" value=\"Add\">" +
                    "    </form></p>");

            out.println("<p><form action=\"Delete\" method=\"post\">");
            out.println("<select name=\"selection\")");

            for (Property i : p1) {
                x = i.getId();
                out.println("<option value=\"" + x + "\">Property " + x + "</option>");
            }

            out.println("</select><input type=\"submit\" value=\"Delete\">");
            out.println("</form></p>");

            out.println("</main></body><footer>Garrett Blazzard | CIT 360 Final | December 2020</footer></html>");

        }
        out.println("</main></body><footer>Garrett Blazzard | CIT 360 Final | December 2020</footer></html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
