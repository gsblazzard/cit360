<%--
  Created by IntelliJ IDEA.
  User: garre
  Date: 12/8/2020
  Time: 4:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Property Manager</title>
    <link rel="stylesheet" href="styles/main.css">
</head>
<body>
<h1>Property Manager</h1>
<nav><a href="index.jsp">Home</a> <a href="contact.html">Contact</a> <a href="about.html">About</a></nav>
<div class="hero"><img src="images/propertyHero.jpeg"> </div>
<main>
    <h2>Welcome Realtor Please Sign In</h2>
    <form action="./signin.html" method="post">
        <input type="submit" value="Sign In">
    </form>
</main>
</body>
<footer>
    Garrett Blazzard | CIT 360 Final | December 2020
</footer>
</html>
