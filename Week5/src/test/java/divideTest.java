import org.junit.*;
import static org.junit.Assert.*;

public class divideTest {

    @Test
    public void testAssertEquals() {
        Divide newDivide = new Divide();
        int x = newDivide.divide(20, 5);
        assertEquals(4, x);
    }

    @Test
    public void testAssertTrue() {
        Divide newDivide = new Divide();
        int x = newDivide.divide(20, 5);
        assertTrue(x == 4);
    }

    @Test
    public void testAssertFalse() {
        Divide newDivide = new Divide();
        int x = newDivide.divide(20, 4);
        assertFalse(x == 4);
    }

    @Test
    public void testAssertNotNull() {
        Divide newDivide = new Divide();
        int x = newDivide.divide(20, 5);
        assertNotNull(x);
    }

    @Test
    public void testAssertSame() {
        Divide newDivide = new Divide();
        int x = newDivide.divide(20, 5);
        int y = newDivide.divide(40, 10);
        assertSame(x, y);
    }

    @Test
    public void testAssertNotSame() {
        Divide newDivide = new Divide();
        int x = newDivide.divide(20, 5);
        int y = newDivide.divide(20, 10);
        assertNotSame(x, y);
    }

    @Test
    public void testAssertArrayEquals() {
        Divide newDivide = new Divide();
        int [] x = new int[] {newDivide.divide(20, 5)};
        int [] y = new int []{newDivide.divide(40, 10)};
        assertArrayEquals(x, y);

    }

}
