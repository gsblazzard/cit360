public class Divide {

    public static void main(String[] args) {
        System.out.println("20 divided by 5 is " + divide(20,5));
    }

    public static int divide(int x, int y){
        return x/y;
    }

}
